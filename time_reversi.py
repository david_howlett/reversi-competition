"""
This function tests how fast the core reversi logic is.
This takes a while so I seperate it from the unit tests.
"""

import time

import reversi

midgame_position = """
. . . o o o o o
. . . . o . o .
. . . . o x x x
. . . x o x o .
. . x x o . o .
. . . . o . . .
. . . . . . . .
. . . . . . . .
"""

board = reversi.parse_board(midgame_position)

for depth in range(10):
    start_time = time.perf_counter()
    reversi.best_move(board, depth, reversi.positional_value1)
    duration = time.perf_counter() - start_time
    print(f"depth {depth} search took {duration*1000:.2f} milliseconds")
