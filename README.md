Reversi Competition
===================


Background
----------

A visitor to my house is learning the game Reversi also known as Orthello. (https://en.wikipedia.org/wiki/Reversi)
I offered to write an AI to play them at Orthello before they left my house.
I want to see if I can write a strong AI from scratch in limited time.


Rules
-----

- The final game must be played before the George (the visitor) leaves my house and I lose if I fail to write the AI in time
- My AI will play against a team of Monin (a lodger with a compsci degree) and George (the visitor)

Extra self imposed constraints for fun
--------------------------------------

- I will write all the code on a Rasbery Pi I just bought (not setup yet)
- I will not lookup the existing literature on how to write Reversi programs. Everything will be derived from scratch.

Result
------

The AI I wrote won the game against the humans.

I had a working AI 13 hours after starting work and played the match against
George and Monim 29 hours after starting work. The AI searches 7-8
moves deep in a few seconds with a negamax tree search with alpha
beta pruning and a valuation function that takes into account the positional
value of pieces.
