"""
Tests for the code in the main reversi file.
Only the tricker bits of the code are tested.
"""


import reversi


def test_legal_moves():
    empty_board = reversi.parse_board(
        """
    . . . . . . . .
    . . . . . . . .
    . . . . . . . .
    . . . . . . . .
    . . . . . . . .
    . . . . . . . .
    . . . . . . . .
    . . . . . . . .
    """
    )
    assert len(list(reversi.legal_moves(empty_board))) == 0
    seperated_pieces = reversi.parse_board(
        """
    . . . . . . . .
    . x x . . o o .
    . x x . . o o .
    . . . . . . . .
    . . . . . . . .
    . . . . . . . .
    . . . . . . . .
    . . . . . . . .
    """
    )
    assert len(list(reversi.legal_moves(seperated_pieces))) == 0
    one_legal_move = reversi.parse_board(
        """
    . . . . . . . .
    . . . . . . . .
    . . . . . . . .
    . . . x o . . .
    . . . . . . . .
    . . . . . . . .
    . . . . . . . .
    . . . . . . . .
    """
    )
    assert list(reversi.legal_moves(one_legal_move)) == [
        reversi.parse_board(
            """
    . . . . . . . .
    . . . . . . . .
    . . . . . . . .
    . . . x x x . .
    . . . . . . . .
    . . . . . . . .
    . . . . . . . .
    . . . . . . . .
    """
        )
    ]
    attack_in_all_directions = reversi.parse_board(
        """
    . . . . . . . .
    . . . . . . . .
    . . . o o o . .
    . . . o x o . .
    . . . o o o . .
    . . . . . . . .
    . . . . . . . .
    . . . . . . . .
    """
    )
    assert len(list(reversi.legal_moves(attack_in_all_directions))) == 8
    attack_in_all_directions2 = reversi.parse_board(
        """
    . . . . . . . .
    . . . . . . . .
    . . . x x x . .
    . . . x o x . .
    . . . x x x . .
    . . . . . . . .
    . . . . . . . .
    . . . . . . . .
    """
    )
    assert len(list(reversi.legal_moves(attack_in_all_directions2))) == 0
    edge_logic = reversi.parse_board(
        """
    o o . . . . o o
    o x . . . . x o
    . . . . . . . .
    . . . . . . . .
    . . . . . . . .
    . . . . . . . .
    o x . . . . x o
    o o . . . . o o
    """
    )
    assert len(list(reversi.legal_moves(edge_logic))) == 0
    edge_logic2 = reversi.parse_board(
        """
    . . . . . . . .
    . o o . . o o .
    . o x . . x o .
    . . . . . . . .
    . . . . . . . .
    . o x . . x o .
    . o o . . o o .
    . . . . . . . .
    """
    )
    assert len(list(reversi.legal_moves(edge_logic2))) == 12
    edge_logic3 = reversi.parse_board(
        """
    x . x . x . . x
    . o o . o . o .
    x o . . . . . .
    . . . . . o o x
    . . . . . . . .
    x o . . . . o x
    . o o . . o o .
    x . x . . x . x
    """
    )
    assert len(list(reversi.legal_moves(edge_logic3))) == 6
    long_attack = reversi.parse_board(
        """
    x . . . . . . x
    o . . o . . o .
    o . . . . o . .
    o . . . o . . .
    o . . o . . . .
    o . o . . . . .
    o o . . . . . .
    . o o o o o o x
    """
    )
    assert list(reversi.legal_moves(long_attack)) == [
        reversi.parse_board(
            """
    x . . . . . . x
    x . . o . . x .
    x . . . . x . .
    x . . . x . . .
    x . . x . . . .
    x . x . . . . .
    x x . . . . . .
    x x x x x x x x
    """
        )
    ]
    assert (
        set(
            reversi.boards_to_strings(
                reversi.legal_moves(
                    reversi.parse_board(
                        """
. . . . . . . .
. . . . . . . .
. . o . . . . .
. . . x o o . .
. . . o o o . .
. . . o . o . .
. . . . . . o .
. . . . . . . .
"""
                    )
                )
            )
        )
        == {
            """
. . . . . . . .
. x . . . . . .
. . x . . . . .
. . . x o o . .
. . . o o o . .
. . . o . o . .
. . . . . . o .
. . . . . . . .
""",
            """
. . . . . . . .
. . . . . . . .
. . o . . . . .
. . . x x x x .
. . . o o o . .
. . . o . o . .
. . . . . . o .
. . . . . . . .
""",
            """
. . . . . . . .
. . . . . . . .
. . o . . . . .
. . . x o o . .
. . . x o o . .
. . . x . o . .
. . . x . . o .
. . . . . . . .
""",
            """
. . . . . . . .
. . . . . . . .
. . o . . . . .
. . . x o o . .
. . . o x o . .
. . . o . x . .
. . . . . . x .
. . . . . . . x
""",
        }
    )


def test_flip_colours():
    assert (
        reversi.board_to_string(
            reversi.flip_colours(
                reversi.parse_board(
                    """
x . x . . . . .
. x o o . . . .
. . o . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
"""
                )
            )
        )
        == """
o . o . . . . .
. o x x . . . .
. . x . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
"""
    )


def test_piece_counts():
    assert (
        reversi.piece_counts(
            reversi.parse_board(
                """
x . x . . . . .
. x o . . . . .
. . o . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
"""
            )
        )
        == (3, 2)
    )


def test_negamax():
    assert reversi.negamax(
        reversi.parse_board(
            """
x o o o o o o o
. x o x x o o x
x x x o x x o x
. . x o o x o x
x x x x x o x x
. x x x x x x x
. . x x x x x .
. . . o . x . .
"""
        ),
        0,
        -9999,
        9999,
        reversi.positional_value2,
    )
    assert reversi.negamax(
        reversi.parse_board(
            """
x o o o o o o o
. x o x x o o x
x x x o x x o x
. . x o o x o x
x x x x x o x x
. x x x x x x x
. . x x x x x .
. . . o . x . .
"""
        ),
        1,
        -9999,
        9999,
        reversi.positional_value2,
    )


def test_best_move():
    assert (
        reversi.board_to_string(
            reversi.best_move(
                reversi.parse_board(
                    """
. . . . . . . .
. o . . . . . .
. . o . . . . .
. . . o . o . .
. . . . o o . .
. . . . . x . .
. . . . . . . .
. . . . . . . .
"""
                ),
                0,
                reversi.positional_value2,
            )[0]
        )
        == """
x . . . . . . .
. x . . . . . .
. . x . . . . .
. . . x . o . .
. . . . x o . .
. . . . . x . .
. . . . . . . .
. . . . . . . .
"""
    )
    assert (
        reversi.board_to_string(
            reversi.best_move(
                reversi.parse_board(
                    """
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . o . .
. . . . o o . .
. . . . . x . .
. . . . . . . .
. . . . . . . .
"""
                ),
                0,
                reversi.positional_value2,
            )[0]
        )
        == """
. . . . . . . .
. . . . . . . .
. . . . . x . .
. . . . . x . .
. . . . o x . .
. . . . . x . .
. . . . . . . .
. . . . . . . .
"""
    )
    assert (
        reversi.board_to_string(
            reversi.best_move(
                reversi.parse_board(
                    """
. . . . . . . .
. . . . . . . .
. . o . . . . .
. . . x o o . .
. . . o o o . .
. . . o . o . .
. . . . . . o .
. . . . . . . .
"""
                ),
                0,
                reversi.positional_value2,
            )[0]
        )
        == """
. . . . . . . .
. . . . . . . .
. . o . . . . .
. . . x o o . .
. . . o x o . .
. . . o . x . .
. . . . . . x .
. . . . . . . x
"""
    )


def test_self_play_mode():
    reversi.self_play_mode(reversi.positional_value1, reversi.positional_value2, search_depth_a=0, search_depth_b=0)
    reversi.self_play_mode(reversi.positional_value1, reversi.positional_value2, search_depth_a=1, search_depth_b=1)
    reversi.self_play_mode(reversi.positional_value1, reversi.positional_value2, search_depth_a=2, search_depth_b=2)
