"""
This is designed to play a game against a human.
The user interface is editing the current state string and running the program.
The program currently plays a move for black.
The board is stored as a 128 bit number, the lower 64 bits record the squares owned by black
and the upper 64 bits record the squares owned by white.

Ideas for improvement:
- negascout
- change the valuation of the squares around the corner after the corner changes ownership
- incorporate move count into board score (an opponent with few moves is in trouble)

To keep track of progress I record the program performance below

negamax + piece counting scoring:
depth 0 search took 0.56 milliseconds
depth 1 search took 0.51 milliseconds
depth 2 search took 5.50 milliseconds
depth 3 search took 39.26 milliseconds
depth 4 search took 329.21 milliseconds
depth 5 search took 2627.37 milliseconds
depth 6 search took 23124.27 milliseconds

negamax + positional scoring:
depth 0 search took 1.09 milliseconds
depth 1 search took 1.03 milliseconds
depth 2 search took 8.85 milliseconds
depth 3 search took 75.25 milliseconds
depth 4 search took 586.36 milliseconds
depth 5 search took 5047.62 milliseconds
depth 6 search took 44983.38 milliseconds

added alpha beta pruning
depth 0 search took 1.68 milliseconds
depth 1 search took 1.07 milliseconds
depth 2 search took 8.80 milliseconds
depth 3 search took 37.85 milliseconds
depth 4 search took 223.77 milliseconds
depth 5 search took 930.77 milliseconds
depth 6 search took 3919.37 milliseconds
depth 7 search took 14712.87 milliseconds

added cache decorator
depth 0 search took 1.41 milliseconds
depth 1 search took 2.30 milliseconds
depth 2 search took 5.41 milliseconds
depth 3 search took 30.14 milliseconds
depth 4 search took 150.76 milliseconds
depth 5 search took 680.32 milliseconds
depth 6 search took 2884.31 milliseconds
depth 7 search took 11419.54 milliseconds

added transposition_table
depth 0 search took 1.12 milliseconds
depth 1 search took 0.48 milliseconds
depth 2 search took 5.10 milliseconds
depth 3 search took 26.89 milliseconds
depth 4 search took 135.41 milliseconds
depth 5 search took 577.39 milliseconds
depth 6 search took 2421.97 milliseconds
depth 7 search took 8939.30 milliseconds

evaluated promising moves first
depth 0 search took 1.14 milliseconds
depth 1 search took 0.49 milliseconds
depth 2 search took 5.09 milliseconds
depth 3 search took 25.62 milliseconds
depth 4 search took 111.49 milliseconds
depth 5 search took 381.82 milliseconds
depth 6 search took 1285.04 milliseconds
depth 7 search took 4947.65 milliseconds
depth 8 search took 15566.44 milliseconds
depth 9 search took 56311.76 milliseconds

use pypy
depth 0 search took 6.01 milliseconds
depth 1 search took 3.03 milliseconds
depth 2 search took 106.02 milliseconds
depth 3 search took 165.75 milliseconds
depth 4 search took 360.35 milliseconds
depth 5 search took 991.20 milliseconds
depth 6 search took 2148.75 milliseconds
depth 7 search took 5799.96 milliseconds
depth 8 search took 15837.82 milliseconds
depth 9 search took 51545.20 milliseconds

Conclusion: pypy not worth much
"""

import functools
import random
import time
from typing import Dict, Generator, List, Tuple, Union

current_state = """
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . x o . . .
. . x x x . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
"""

MINIMUM_SEARCH_TIME = 3

BLACK = "x"
WHITE = "o"
EMPTY = "."

# These are for moving positions within an int that represents a board
RIGHT = 1
LEFT = -1
DOWN = 8
UP = -8

# When calculating which pieces to flip, we must avoid
# going off the edge of the board. I pre-calculate a
# lookup table here for speed.
ITTERATION_DISTANCES = (8, 7, 6, 5, 4, 3, 0, 0)
DISTANCES_TO_ITTERATE_TO = {
    RIGHT: ITTERATION_DISTANCES * 8,
    LEFT: ITTERATION_DISTANCES[::-1] * 8,
    DOWN: [distance for distance in ITTERATION_DISTANCES for _ in range(8)],
    UP: [distance for distance in ITTERATION_DISTANCES[::-1] for _ in range(8)],
}
DISTANCES_TO_ITTERATE_TO[LEFT + UP] = [
    min(x) for x in zip(DISTANCES_TO_ITTERATE_TO[LEFT], DISTANCES_TO_ITTERATE_TO[UP])
]
DISTANCES_TO_ITTERATE_TO[LEFT + DOWN] = [
    min(x) for x in zip(DISTANCES_TO_ITTERATE_TO[LEFT], DISTANCES_TO_ITTERATE_TO[DOWN])
]
DISTANCES_TO_ITTERATE_TO[RIGHT + UP] = [
    min(x) for x in zip(DISTANCES_TO_ITTERATE_TO[RIGHT], DISTANCES_TO_ITTERATE_TO[UP])
]
DISTANCES_TO_ITTERATE_TO[RIGHT + DOWN] = [
    min(x) for x in zip(DISTANCES_TO_ITTERATE_TO[RIGHT], DISTANCES_TO_ITTERATE_TO[DOWN])
]


def value_lookup(positional_values_for_a_corner, x: int, y: int):
    if x > 3:
        x = 7 - x
    if y > 3:
        y = 7 - y
    if y > x:
        x, y = y, x
    return positional_values_for_a_corner[y][x]


# fmt: off
positional_values_for_a_corner1 = [
    [100,   -40,   30, 20],
    [None,  -50,  -10, -5],
    [None, None,    5,  5],
    [None, None, None,  5],
]
positional_values_for_a_corner2 = [
    [100,   -40,   30, 20],
    [None,  -50,  -10, -5],
    [None, None,    5,  5],
    [None, None, None,  5],
]

DIRECTIONS = [
    LEFT+UP,   UP,   RIGHT+UP,
    LEFT,            RIGHT,
    LEFT+DOWN, DOWN, RIGHT+DOWN
]
# fmt: on
positional_values1 = [value_lookup(positional_values_for_a_corner1, x, y) for x in range(8) for y in range(8)]
positional_values2 = [value_lookup(positional_values_for_a_corner2, x, y) for x in range(8) for y in range(8)]
transposition_table: Dict[int, Tuple[float, str, int]] = {}


def parse_board(board_string: str) -> int:
    board_string = board_string.replace(" ", "").replace("\n", "")
    assert len(board_string) == 64
    board = sum(1 << position for (position, square) in enumerate(board_string) if square == BLACK)
    board += sum(1 << (position + 64) for (position, square) in enumerate(board_string) if square == WHITE)
    return board


def board_to_string(board: int) -> str:
    board_string = "\n".join(
        " ".join(
            BLACK if board & (1 << (square + line * 8)) else WHITE if board & (1 << (square + line * 8 + 64)) else EMPTY
            for square in range(8)
        )
        for line in range(8)
    )
    return "\n" + board_string + "\n"


def boards_to_strings(boards: List[int]) -> List[str]:
    return [board_to_string(board) for board in boards]


def print_board(board: int):
    print(board_to_string(board))


def flip_colours(board: int) -> int:
    new_blacks = board >> 64
    new_whites = (board & ((1 << 64) - 1)) << 64
    return new_blacks | new_whites


def legal_moves(board: int) -> Generator[int, None, None]:
    # the lower 64 bits record whether the squares are have a (black or white) piece on
    occupied_squares = board | (board >> 64)
    # loop over all possible squares that a piece could be placed
    # and figure out which ones are allowed
    for position in range(64):
        bit_position = 1 << position
        # it is illegal to place a piece on another piece
        # this allows fast termination of the loop
        if bit_position & occupied_squares:
            continue
        new_board = board
        for direction in DIRECTIONS:
            squares_to_flip = 0
            for i in range(1, DISTANCES_TO_ITTERATE_TO[direction][position]):
                if direction > 0:
                    position_of_interest = bit_position << (direction * i)
                else:
                    position_of_interest = bit_position >> (-direction * i)

                # todo there is a possible optimisation from pulling the if outside the loop
                if i == 1:
                    # if we don't hit a white square on the first step, quit early
                    if not (position_of_interest << 64) & board:
                        break
                    squares_to_flip |= position_of_interest
                else:
                    # if we hit a black square we should save the squares to flip and quit
                    if position_of_interest & board:
                        # add the black squares and remove the white squares
                        new_board ^= squares_to_flip
                        # remove the white squares
                        new_board ^= squares_to_flip << 64
                        break
                    # if we hit a white square we should keep going
                    elif (position_of_interest << 64) & board:
                        squares_to_flip |= position_of_interest
                    # otherwise we should quit
                    else:
                        break
        if new_board == board:
            # then the move would flip no pieces so it is invalid
            continue
        # now that we know that placing a piece is legal,
        # we place the piece and yield
        yield new_board | bit_position


@functools.lru_cache(maxsize=10_000)
def positional_value1(board):
    score = 0
    game_progress = bin(board).count("1") / 64
    game_progress **= 1.5
    for position in range(64):
        black_bit_position = 1 << position
        white_bit_position = 1 << (position + 64)
        # check for black squares
        if board & black_bit_position:
            score += game_progress + (1 - game_progress) * positional_values1[position]
        # check for black squares
        elif board & white_bit_position:
            score -= game_progress + (1 - game_progress) * positional_values1[position]
        # todo count moves that can be made
        else:
            pass
    return score


@functools.lru_cache(maxsize=10_000)
def positional_value2(board):
    score = 0
    game_progress = bin(board).count("1") / 64
    game_progress **= 1.5
    for position in range(64):
        black_bit_position = 1 << position
        white_bit_position = 1 << (position + 64)
        # check for black squares
        if board & black_bit_position:
            score += game_progress + (1 - game_progress) * positional_values1[position]
        # check for black squares
        elif board & white_bit_position:
            score -= game_progress + (1 - game_progress) * positional_values1[position]
        # todo count moves that can be made
        else:
            pass
            # for direction in DIRECTIONS:
            #    for i in range(1, DISTANCES_TO_ITTERATE_TO[direction][position]):
            #        if direction > 0:
            #            position_of_interest = black_bit_position << (direction * i)
            #        else:
            #            position_of_interest = black_bit_position >> (-direction * i)
            #
            #        # todo there is a possible optimisation from pulling the if outside the loop
            #        if i == 1:
            #            # if we don't hit a white square on the first step, quit early
            #            if not (position_of_interest << 64) & board:
            #                break
            #        else:
            #            # if we hit a black square we should record the square as a valid move for black
            #            if position_of_interest & board:
            #                valid_moves_for_black += 1
            #                break
            #            # if we hit a white square we should keep going
            #            elif (position_of_interest << 64) & board:
            #                pass
            #            # otherwise we should quit
            #            else:
            #                break

    # This increases win rate to about 60% but it is very expensive to run
    # valid_moves_for_black = len(list(legal_moves(board)))
    # valid_moves_for_white = len(list(legal_moves(flip_colours(board))))
    # score += 10*math.sqrt(valid_moves_for_black)
    # score -= 10*math.sqrt(valid_moves_for_white)
    return score


def piece_value(board):
    return bin((board & ((1 << 64) - 1))).count("1") - bin(board >> 64).count("1")


def piece_counts(board: int) -> Tuple[int, int]:
    """Returns the number of pieces black is winning by"""
    white_pieces = bin(board >> 64).count("1")
    black_pieces = bin((board & ((1 << 64) - 1))).count("1")
    return black_pieces, white_pieces


def negamax(board: int, depth: int, alpha: float, beta: float, value_func) -> float:
    """Implements negamax tree search, returning a score."""
    original_alpha = alpha

    # lookup the current node to see if it has already been searched
    if board in transposition_table:
        node_score, node_type, node_search_depth = transposition_table[board]
        if node_search_depth >= depth:
            if node_type == "exact":
                return node_score
            elif node_type == "low":
                alpha = max(alpha, node_score)
            elif node_type == "high":
                beta = min(beta, node_score)

            if alpha > beta:
                return node_score

    if depth <= 0:
        return value_func(board)

    moves: Union[List[int], Generator[int, None, None]] = legal_moves(board)
    if depth > 2:
        # When using alpha-beta search, trying the best moves first improves the efficiency of the search.
        # The issue is that with the current design I need to generate moves to order them and generation is expensive.
        # The compromise is to skip move ordering near the leaves.
        moves = list(moves)
        moves.sort(
            key=lambda _move: -(transposition_table[_move][0] if _move in transposition_table else value_func(_move))
        )
    current_best_score = -999997.0
    no_moves = True
    for move in moves:
        no_moves = False
        if depth > 1:
            move_score = -negamax(flip_colours(move), depth - 1, -beta, -alpha, value_func)
        else:
            move_score = value_func(board)
        current_best_score = max(current_best_score, move_score)
        alpha = max(alpha, current_best_score)
        if alpha >= beta:
            break
    # If there are no valid moves, the other guy still gets a turn
    # I suspect it is good to search to the original depth
    if no_moves:
        if depth > 2:
            current_best_score = -negamax(flip_colours(board), depth - 1, -beta, -alpha, value_func)
        else:
            current_best_score = value_func(board)
    assert abs(current_best_score) < 1000
    if current_best_score <= original_alpha:
        node_type = "high"
    elif current_best_score >= beta:
        node_type = "low"
    else:
        node_type = "exact"
    transposition_table[board] = current_best_score, node_type, depth
    return current_best_score


def best_move(board: int, search_depth: int, value_func, randomise: int = 0) -> Tuple[int, float]:
    best_move_ = board  # this is a good default for when there are no legal moves
    best_score = -99998.0
    moves: Union[List[int], Generator[int, None, None]] = legal_moves(board)

    if search_depth > 2:
        # When using alpha-beta search, trying the best moves first improves the efficiency of the search.
        # The issue is that with the current design I need to generate moves to order them and generation is expensive.
        # The compromise is to skip move ordering near the leaves.
        moves = list(moves)
        moves.sort(
            key=lambda _move: -(transposition_table[_move][0] if _move in transposition_table else value_func(_move))
        )

    no_moves = True
    for move in moves:
        no_moves = False
        score = -negamax(flip_colours(move), search_depth - 1, -9999.0, 9999.0, value_func)
        # this randomisation allows the comparison of value functions
        if randomise:
            score += random.randint(-randomise, randomise)
        if score > best_score:
            best_move_ = move
            best_score = score

    # If there are no valid moves, the other guy still gets a turn
    # I try to search to the original depth. The flipping of the colours looks wrong but is correct.
    # It looks odd because the search depth is reduced compared to normal but this is due to the turn number
    # incrementing twice due to the missed move.
    if no_moves:
        if search_depth > 2:
            best_score = -negamax(flip_colours(board), search_depth - 2, -9999.0, 9999.0, value_func)
        else:
            best_score = value_func(board)
    return best_move_, best_score


def play_one_move(board: int, value_func, player_is_black=True, print_state=True, randomise=0, search_depth=None):
    start_time = time.perf_counter()
    if search_depth is None:
        for search_depth in range(20):
            if player_is_black:
                new_board, score = best_move(board, search_depth, value_func, randomise)
            else:
                inverted_board, inverted_score = best_move(flip_colours(board), search_depth, value_func, randomise)
                new_board, score = flip_colours(inverted_board), -inverted_score
            search_time = time.perf_counter() - start_time
            if print_state:
                print_board(new_board)
                print(f"Search depth: {search_depth}")
                print(f"Search time: {search_time:.2f}")
                print(f"Estimated score for black: {score:.1f}")
            if search_time > MINIMUM_SEARCH_TIME:
                break
    else:
        if player_is_black:
            new_board, score = best_move(board, search_depth, value_func, randomise)
        else:
            inverted_board, inverted_score = best_move(flip_colours(board), search_depth, value_func, randomise)
            new_board, score = flip_colours(inverted_board), -inverted_score
        search_time = time.perf_counter() - start_time
        if print_state:
            print_board(new_board)
            print(f"Search depth: {search_depth}")
            print(f"Search time: {search_time:.2f}")
            print(f"Estimated score for black: {score:.1f}")
    return new_board


def single_player_mode(value_func, player_is_black=True):
    board = parse_board(current_state)
    play_one_move(board, value_func, player_is_black=player_is_black)


def self_play_mode(
    value_func_a, value_func_b, search_depth_a=None, search_depth_b=None, print_state=True, randomise=0
) -> float:
    board = parse_board(current_state)
    # print_board(board)
    history = [0]
    player_is_black = True
    while True:
        history.append(board)
        board = play_one_move(
            board,
            value_func_a if player_is_black else value_func_b,
            player_is_black,
            search_depth=search_depth_a if player_is_black else search_depth_b,
            print_state=print_state,
            randomise=randomise,
        )
        if board == history[-2]:
            break
        player_is_black = not player_is_black
    black_piece_count, white_piece_count = piece_counts(board)
    if print_state:
        print("black piece count", black_piece_count)
        print("white piece count", white_piece_count)
    if black_piece_count > white_piece_count:
        if print_state:
            print("black won")
        return 1
    elif black_piece_count < white_piece_count:
        if print_state:
            print("white won")
        return 0
    else:
        if print_state:
            print("draw")
        return 0.5


def tournament(value_func_a, value_func_b, search_depth_a=None, search_depth_b=None):
    wins = 0
    games = 0
    for i in range(1000):
        result = self_play_mode(
            value_func_a,
            value_func_b,
            print_state=True,
            randomise=1,
            search_depth_a=search_depth_a,
            search_depth_b=search_depth_b,
        )
        games += 1
        wins += result
        print(f"Game {i} score was {result} overall black percentage won: {100*wins/games:.1f}%")


if __name__ == "__main__":
    # tournament(positional_value2, positional_value1, search_depth_a = 0, search_depth_b = 0)
    single_player_mode(positional_value2, player_is_black=False)
    # self_play_mode(positional_value2, positional_value1)
